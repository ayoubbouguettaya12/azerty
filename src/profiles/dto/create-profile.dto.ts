import { ApiProperty } from '@nestjs/swagger';
import {
  IsInt,
  IsNotEmpty,
  IsString,
  Min,
} from 'class-validator';


export class CreateProfileDto {

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;
  
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  clearanceType: string;
  
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  position: string;
  
  @ApiProperty()
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  age: number;
  
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  height: string;
  
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  weight: string;
  
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  phone: string;
  
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  email: string;
  
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  image: string;

}