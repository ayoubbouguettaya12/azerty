import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsInt, IsOptional, IsString } from 'class-validator';
import { Type } from 'class-transformer';

enum Sort {
    'ASC' = 'asc',
    'DESC' = 'desc',
}

export class GetFilterdProfileDto {
    @ApiProperty({ example: 10 })
    @IsInt()
    @IsOptional()
    @Type(() => Number)
    size: number;

    @ApiProperty({ example: 1 })
    @IsInt()
    @IsOptional()
    @Type(() => Number)
    page: number;

    @IsEnum(Sort)
    @IsString()
    @IsOptional()
    'name': Sort;

}