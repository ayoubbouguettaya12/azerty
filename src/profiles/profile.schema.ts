import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type ProfileDocument = Profile & mongoose.Document;

@Schema()
export class Profile {
  _id: string;

  @Prop()
  name: string;
  @Prop()
  clearanceType: string;
  @Prop()
  position: string;
  @Prop()
  age: number;
  @Prop()
  height: string;
  @Prop()
  weight: string;
  @Prop()
  phone: string;
  @Prop()
  email: string;
  @Prop()
  image: string;
}

export const ProfileSchema = SchemaFactory.createForClass(Profile);