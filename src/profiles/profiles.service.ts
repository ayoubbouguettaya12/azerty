import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { Profile, ProfileDocument } from './profile.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { profile } from 'console';
import { GetFilterdProfileDto } from './dto/get-profile-filters.dto';

@Injectable()
export class ProfilesService {
  constructor(@InjectModel(Profile.name)
  protected ProfileModel: Model<ProfileDocument>,
  ) { }

  private logger = new Logger(ProfilesService.name)

  async profiles_get(filters: GetFilterdProfileDto) {
    try {
      this.logger.verbose(`[profiles_get] Getting all profiles.`);

      const {
        name = "",
        page,
        size,
      } = filters;

      let query = {}
      if (name) query = { name };

      const profiles = await this.ProfileModel
        .find({ query })
        .skip(size * (page - 1))
        .limit(size)
        .exec();


      // Number of edges
      const count = await this.ProfileModel.count(query);
      // Maximum pages to get
      const maxPages = Math.ceil(count / size);

      return {
        items: profiles,
        pageLength: profiles.length,
        page: page,
        maxPages: maxPages,
        size: size,
        totalCount: count,
      };
    } catch (error) {
      this.logger.error(`[profiles_get] ${error.message}.`);
      return error;
    }
  };


  async profiles_profile_get(id: string) {
    try {
      this.logger.verbose(`[profiles_profile_get] Getting profile for ${id}..`);
      const profile = await this.ProfileModel.findById(id)

      if (!profile) {
        throw {
          error: "profile NOT FOUND",
          message: `profile : ${id} Not found`,
          statusCode: HttpStatus.NOT_FOUND
        }
      }
      return profile;
    } catch (error) {
      this.logger.error(`[profiles_profile_get] ${error.message}.`);
      return error;
    }
  };


  async profiles_profile_delete(id: string) {
    try {
      this.logger.verbose(`[profiles_profile_delete] Getting profile for ${id}..`);
      const profile = await this.ProfileModel.findByIdAndDelete(id)

      if (!profile) {
        throw {
          error: "profile NOT FOUND",
          message: `profile : ${id} Not found`,
          statusCode: HttpStatus.NOT_FOUND
        }
      }
      return profile;

    } catch (error) {
      this.logger.error(`[profiles_profile_delete] ${error.message}.`);
    }
  };

  async profiles_profile_post(profile: CreateProfileDto) {
    try {
      this.logger.verbose(`[profiles_profile_post] Inserting profile`);
      return await this.ProfileModel.create(profile)
    } catch (error) {
      this.logger.error(`[profiles_profile_post] ${error.message}.`);
    }
  };

  /**
* profiles_profile_put
*
* PUT: /api/facial-recognition/profiles/profile
* 
*/

  async profiles_profile_put(id: string, profile: UpdateProfileDto) {
    try {
      this.logger.verbose(`[profiles_profile_put] Updating profile ${id}..`);
      const updatedProfile = await this.ProfileModel.findByIdAndUpdate(id, profile)

      if (!updatedProfile) {
        throw {
          error: "profile NOT FOUND",
          message: `profile : ${id} Not found`,
          statusCode: HttpStatus.NOT_FOUND
        }
      }
      return updatedProfile;
    } catch (error) {
      this.logger.error(`[profiles_profile_put] ${error.message}.`);
    }
  };

}
