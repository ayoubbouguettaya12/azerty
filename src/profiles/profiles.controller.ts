import { Controller, Get, Post, Body, Patch, Param, Delete, Put, Query, ValidationPipe, UsePipes } from '@nestjs/common';
import { ProfilesService } from './profiles.service';
import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { GetFilterdProfileDto } from './dto/get-profile-filters.dto';

@Controller('profiles')
@ApiTags('Profiles')
export class ProfilesController {
  constructor(private readonly profilesService: ProfilesService) { }

  @Get()
  @UsePipes(new ValidationPipe({ transform: true }))
  @ApiOperation({ summary: 'get all profiles ' })
  @ApiQuery({ name: 'page', required: false })
  @ApiQuery({ name: 'size', required: false })
  @ApiQuery({ name: 'name', required: false })
  findAll(
    @Query() filters: GetFilterdProfileDto
  ) {
    return this.profilesService.profiles_get(filters);
  }


  @Get('/profile/:id')
  @ApiOperation({ summary: 'get one Profile' })
  findOne(@Param('id') id: string) {
    return this.profilesService.profiles_profile_get(id);
  }

  @Delete('/profile/:id')
  @ApiOperation({ summary: 'delete one Profile' })
  deleteOne(@Param('id') id: string) {
    return this.profilesService.profiles_profile_delete(id);
  }

  @Post('/profile')
  @ApiOperation({ summary: 'create Profile' })
  CreateOne(@Body() profileBody: CreateProfileDto) {
    return this.profilesService.profiles_profile_post(profileBody);
  }

  @Put('/profile/:id')
  @ApiOperation({ summary: 'update Profile' })
  UpdateOne(@Param('id') id: string, @Body() profileBody: UpdateProfileDto) {
    return this.profilesService.profiles_profile_put(id,profileBody);
  }

}
