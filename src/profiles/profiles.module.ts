import { Module } from '@nestjs/common';
import { ProfilesService } from './profiles.service';
import { ProfilesController } from './profiles.controller';
import { FileController } from './file/file.controller';
import { FileService } from './file/file.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Profile, ProfileSchema } from './profile.schema';

@Module({
  imports: [

    MongooseModule.forFeature([
      { name: Profile.name, schema: ProfileSchema }
    ]),
  ],
  controllers: [ProfilesController, FileController],
  providers: [ProfilesService, FileService]
})
export class ProfilesModule {}
