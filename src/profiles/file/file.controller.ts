import { Controller, Get, Param } from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { FileService } from './file.service';

@Controller('file')
export class FileController {
    constructor(private fileSevice: FileService) { }

    @Get(':path')
    @ApiOperation({ summary: 'download logo image' })
    async getFile(
        @Param('path') path: string
    ) {
        return this.fileSevice.file_get(path)
    }
}
