import { HttpStatus, Injectable, StreamableFile } from '@nestjs/common';
import path from 'path';

import { access } from 'fs/promises'
import { createReadStream, constants as fsConstants } from 'fs';

const mimeTypes = {
    "html": "text/html",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "png": "image/png",
    "js": "text/javascript",
    "css": "text/css"
};

const publicDocumentDir = 'public';

@Injectable()
export class FileService {

    private fileExists = async (path) =>
        access(path, fsConstants.F_OK)
            .then(() => true)
            .catch(() => false);

    async file_get(fileName) {
        try {
            const filePath = path.join(process.cwd(), publicDocumentDir, decodeURI(fileName));

            if (!(await this.fileExists(filePath))) {
                throw {
                    error: "FILE NOT FOUND",
                    message: `FILE ${decodeURI(fileName)} `,
                    statusCode: HttpStatus.NOT_FOUND
                }
            }
            const file = createReadStream(filePath);
            return new StreamableFile(file);
        } catch (error) {
            return error;
        }

    };
}


