import {
    Injectable,
    OnModuleInit,
    OnApplicationBootstrap,
} from '@nestjs/common';
import { Aedes } from 'aedes';
import aedes from 'aedes';
import { createServer } from 'aedes-server-factory';
import { protocolDecoder } from 'aedes-protocol-decoder';
import { PublishPacket } from 'packet';
import { ChildProcess } from 'child_process';

@Injectable()
export class BrokerService implements OnModuleInit, OnApplicationBootstrap {
    private aedesInstance: Aedes;

    constructor() { }

    onModuleInit() {
        console.log("====================onINITMODULE================================")
        this.aedesInstance = new aedes({});
    }

    onApplicationBootstrap() {
        console.log("====================onApplicationBootstrap================================")
        createServer(this.aedesInstance, {
            trustProxy: true,
            protocolDecoder,
            ws: true,
        }).listen(1884, () => {
            console.log('MQTT Broker is listening on port 1884');
        });


        this.aedesInstance.on('client', (client) => {
            console.log(`Client ${client.id} connected`);
        })

        this.aedesInstance.on('subscribe', (topic, client) => {
            console.log(`Client ${client.id} subscribed to ${topic}.`);
        })

        this.aedesInstance.on('unsubscribe', (topic, client) => {
            console.log(`Client ${client.id} unsubscribed from ${topic}.`);
        })

        this.aedesInstance.on('clientDisconnect', (client) => {
            console.log(`Client ${client.id} disconnet`);
        })

        this.aedesInstance.on('publish', (packet, client) => {
            console.log(`Published to ${packet.topic} <- ${packet.payload}`);
        })

    }

    getAedesInstance() {
        return this.aedesInstance
    }
}