import { ApiProperty } from '@nestjs/swagger';
import {
  IsInt,
  IsNotEmpty,
  IsString,
  Min,
} from 'class-validator';


export class PublishMQTTDto {

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  topic: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  message: string;

}