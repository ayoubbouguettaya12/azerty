import { Injectable, Logger } from '@nestjs/common';
import { BrokerService } from './broker.service';
import { PublishPacket } from 'packet';

@Injectable()
export class MqttService {

    constructor(private brokerService: BrokerService) {
    }

    private logger = new Logger("MQTT SERVICE")

    Publish(topic: string, message: string) {
        const payload = {
            topic: topic,
            payload: message,
            qos: 0,
            retain: false
        } as PublishPacket;


        this.brokerService.getAedesInstance().publish(payload, (error) => {
            if (!error) {
                this.logger.verbose("[Publish] Message Published Successfully");
                return
            }
            this.logger.error(`[Publish] ${error}`)
        })
    }

}
