import { Module } from '@nestjs/common';
import { MqttController } from './mqtt.controller';
import { MqttService } from './mqtt.service';
import { BrokerService } from './broker.service';

@Module({
  controllers: [MqttController],
  providers: [MqttService,BrokerService]
})
export class MqttModule {}
