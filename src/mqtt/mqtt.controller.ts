import { Body, Controller, Post } from '@nestjs/common';
import { MqttService } from './mqtt.service';
import { ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger';
import { PublishMQTTDto } from './publish-mqtt.dto';

@Controller('mqtt')
@ApiTags('MQTT')
export class MqttController {
    constructor(private readonly mqttService: MqttService) { }

    @Post('')
    @ApiOperation({ summary: 'Publish new Record on MQTT SERVER' })
    @ApiBody({ type: PublishMQTTDto})
    CreateOne(@Body('topic') topic: string, @Body('message') message: string,) {
        return this.mqttService.Publish(topic, message);
    }
}
