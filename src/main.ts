import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const host = 'localhost';
  const port = 8000;

  const config = new DocumentBuilder()
    .setTitle('Intel Facial Recognition Server ')
    .setDescription('Intel Facial Recognition Server ')
    .setVersion('0.0.1')
    .addBearerAuth()
    .addServer(`http://${host}:${port}/api/facial-recognition`, 'server url ')
    .setDescription(``)
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/', app, document);

  app.setGlobalPrefix('/api/facial-recognition')

  app.enableCors();
  app.useGlobalPipes(new ValidationPipe({}));

  await app.listen(8000);
}
bootstrap();
