import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProfilesModule } from './profiles/profiles.module';
import { MqttModule } from './mqtt/mqtt.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ProfilesModule,
     MqttModule,
     MongooseModule.forRoot(
      process.env.NODE_ENV === 'production'
        ? `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}`
        // : 'mongodb://root:example@10.5.131.142:27017/', // for testing
      : 'mongodb://localhost:27017/', // for testing
    ),
    ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
